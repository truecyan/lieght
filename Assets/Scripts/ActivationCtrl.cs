﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ActivationCtrl : MonoBehaviour
{
    public List<LightMap> LightList;
    public List<LightMap> DarkLightList;

    public ElementCtrl[] ActivationSeq = {null};
    public ElementCtrl DarkObj = null;

    [SerializeField] private int _index;

    private Collider2D _col;
    private void Start()
    {
        _col = GetComponent<Collider2D>();
        
        foreach (var elem in ActivationSeq)
        {
            if(elem) elem.SetActive(false);
        }

        if (DarkObj) DarkObj.SetActive(true);
    }
    public void Activate()
    {
        Debug.Log("Activate: " + gameObject);

        if (DarkLightList.Count > 0) return;

        _index = (_index + 1) % ActivationSeq.Length;
        if(DarkObj) DarkObj.SetActive(false);
        if(ActivationSeq[_index]) ActivationSeq[_index].SetActive(true);
        LightManager.Instance.MapUpdate();
    }
    public void Deactivate()
    {
        Debug.Log("Deactivate: " + gameObject);

        if (ActivationSeq[_index]) ActivationSeq[_index].SetActive(false);
        if (DarkObj) DarkObj.SetActive(true);
        LightManager.Instance.MapUpdate();
    }

    public void AddLight(LightMap lightMap)
    {
        if(LightList.Count <= 0) Activate();
        if(!LightList.Contains(lightMap)) LightList.Add(lightMap);
    }

    public void DeleteLight(LightMap lightMap)
    {
        LightList.Remove(lightMap);
        if(LightList.Count <= 0) Deactivate();
    }

    public void AddDarkLight(LightMap lightMap)
    {
        //if (LightList.Count <= 0 && DarkLightList.Count <= 0) DarkActivate();
        if (!DarkLightList.Contains(lightMap)) DarkLightList.Add(lightMap);
    }

    public void DeleteDarkLight(LightMap lightMap)
    {
        DarkLightList.Remove(lightMap);
        //if(LightList.Count <= 0 && DarkLightList.Count <= 0) DarkDeactivate();
    }
}
