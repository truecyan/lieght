﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.UI;

public class LightManager : MonoBehaviour
{
    public struct LightInfo
    {
        public int startIndex;
        public Vector2 origin;
        public Vector2 down;
        public float alpha;

        public LightInfo(int idx, Vector2 originPos, Vector2 downVec, float alphaVal)
        {
            startIndex = idx;
            origin = originPos;
            down = downVec;
            alpha = alphaVal;
        }
    }

    private static LightManager _instance = null;
    public static LightManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<LightManager>();
                /*
                if (_instance == null)
                {
                    var Object = Instantiate(Resources.Load<GameObject>("_GameManager/LightManagerCanvas"));
                    _instance = Object.GetComponentInChildren<LightManager>();
                }*/
            }
            return _instance;
        }
    }

    public List<LightMap> Lights;
    public Camera DarkCamera;
    public ComputeShader initShader;
    public ComputeShader shader;
    public float fading = 0.5f;

    public delegate void OnSwitchUpdate(string symbol, bool state);
    public event OnSwitchUpdate SwitchUpdateEvent;

    public void SwitchUpdate(string symbol, bool state)
    {
        SwitchUpdateEvent?.Invoke(symbol, state);
    }
    
    private RenderTexture _tex;
    private RenderTexture _cameraTex;

    private RawImage _img;

    private bool _isUpdated;

    public void MapUpdate()
    {
        _isUpdated = true;
    }

    public bool CheckUpdate()
    {
        var res = _isUpdated;
        _isUpdated = false;
        return res;
    }
    private void LightUpdate()
    {
        DarkCamera.Render();

        //var lightPolygons = new List<PolygonLayer>();
        var lightPos = new List<Vector2>();
        var lightInfo = new List<LightInfo>();

        foreach (var light in Lights)
        {
            var idx = lightPos.Count;
            var originPos = light.GetOrigin();
            lightInfo.Add(new LightInfo(idx, originPos, -light.transform.up, light.Alpha));
            foreach (var pos in light.GetLight())
            {
                lightPos.Add(pos);
            }
        }

        int width = _tex.width;
        int height = _tex.height;

        int initKernel = initShader.FindKernel("Initialize");
        initShader.SetTexture(initKernel, "Source", _cameraTex);
        initShader.SetTexture(initKernel, "Result", _tex);

        initShader.Dispatch(initKernel, width / 8, height / 8, 1);

        if (lightInfo.Count <= 0) return;

        var posArray = lightPos.ToArray();
        var infoArray = lightInfo.ToArray();
        ComputeBuffer posBuffer = new ComputeBuffer(posArray.Length, 8);
        ComputeBuffer infoBuffer = new ComputeBuffer(infoArray.Length, 24);
        posBuffer.SetData(posArray);
        infoBuffer.SetData(infoArray);

        int kernel = shader.FindKernel("SightCompute");
        Vector2 origin = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        Vector2 xOne = Camera.main.ScreenToWorldPoint(new Vector2(1, 0));
        float stride = xOne.x - origin.x;
        shader.SetBuffer(kernel, "PosBuffer", posBuffer);
        shader.SetBuffer(kernel, "InfoBuffer", infoBuffer);
        shader.SetInt("size", posArray.Length);
        shader.SetInt("infoSize", infoArray.Length);
        shader.SetFloats("origin", origin.x, origin.y);
        shader.SetFloat("stride", stride);
        shader.SetFloat("fading", fading);
        shader.SetTexture(kernel, "Result", _tex);

        shader.Dispatch(kernel, width / 8, height / 8, infoArray.Length);

        posBuffer.Dispose();
        infoBuffer.Dispose();
    }
    // Start is called before the first frame update
    void Start()
    {
        _img = GetComponent<RawImage>();
        _img.enabled = true;
        _tex = new RenderTexture(Screen.width, Screen.height, 24) {enableRandomWrite = true};
        _tex.Create();
        _img.texture = _tex;

        _cameraTex = new RenderTexture(Screen.width, Screen.height, 24);
        _cameraTex.Create();
        DarkCamera.targetTexture = _cameraTex;
    }

    // Update is called once per frame
    void Update()
    {
        LightUpdate();
    }
}
