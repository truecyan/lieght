﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampActivation : MonoBehaviour
{
    public bool DarkActivation = true;
    public List<bool> ActivationPattern;
    public GameObject Visual;
    public GameObject Dark;
    private int _activationIndex;
    public LampCtrlOld Lamp;
    [SerializeField] private int _lightCount;

    private void Start()
    {
        Dark.SetActive(DarkActivation);
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (ActivationPattern.Count <= 0 || _lightCount == 0) return;
        if (hasFocus)
        {
            _activationIndex = (_activationIndex + 1) % ActivationPattern.Count;
            Visual.SetActive(ActivationPattern[_activationIndex]);
            gameObject.layer = ActivationPattern[_activationIndex]
                ? LayerMask.NameToLayer("Enabled")
                : LayerMask.NameToLayer("Disabled");
            Lamp.SetActivation(ActivationPattern[_activationIndex]);
            if (!ActivationPattern[_activationIndex]) Lamp.DestroyRope();
            else Lamp.Hang();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (ActivationPattern.Count <= 0) return;
        if (!other.CompareTag("Light")) return;
        _lightCount += 1;
        if (_lightCount != 1) return;

        Visual.SetActive(ActivationPattern[_activationIndex]);
        gameObject.layer = ActivationPattern[_activationIndex]
            ? LayerMask.NameToLayer("Enabled")
            : LayerMask.NameToLayer("Disabled");
        Lamp.SetActivation(DarkActivation);
        if (!ActivationPattern[_activationIndex]) Lamp.DestroyRope();
        else Lamp.Hang();

        if (!ActivationPattern[_activationIndex]) return;

        Collider2D[] cols = new Collider2D[1];
        var filter = new ContactFilter2D();
        filter.SetLayerMask(LayerMask.GetMask("Character"));
        GetComponent<Collider2D>().OverlapCollider(filter, cols);
        if (cols[0])
        {
            Visual.SetActive(false);
            gameObject.layer = LayerMask.NameToLayer("Disabled");
            Lamp.SetActivation(false);
            Lamp.DestroyRope();
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (ActivationPattern.Count <= 0) return;
        if (!other.CompareTag("Light")) return;
        _lightCount -= 1;
        if (_lightCount != 0) return;
        _activationIndex = (_activationIndex + 1) % ActivationPattern.Count;
        Visual.SetActive(DarkActivation);
        gameObject.layer = DarkActivation
            ? LayerMask.NameToLayer("Enabled")
            : LayerMask.NameToLayer("Disabled");
        Lamp.SetActivation(DarkActivation);
        if (!ActivationPattern[_activationIndex]) Lamp.DestroyRope();
        else Lamp.Hang();

        if (!DarkActivation) return;

        Collider2D[] cols = new Collider2D[1];
        var filter = new ContactFilter2D();
        filter.SetLayerMask(LayerMask.GetMask("Character"));
        GetComponent<Collider2D>().OverlapCollider(filter, cols);
        if (cols[0])
        {
            Visual.SetActive(false);
            gameObject.layer = LayerMask.NameToLayer("Disabled");
            Lamp.SetActivation(false);
            Lamp.DestroyRope();
        }
    }
}
