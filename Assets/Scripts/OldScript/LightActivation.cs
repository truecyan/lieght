﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightActivation : MonoBehaviour
{
    public bool DarkActivation = true;
    public List<bool> ActivationPattern;
    public GameObject Visual;
    public GameObject Dark;
    [SerializeField] private int _activationIndex;
    private bool _enabledFrame;
    private int _lightCount;


    private void Start()
    {
        if(!Dark) Debug.Log(gameObject);
        Dark.SetActive(DarkActivation);
        Visual.SetActive(DarkActivation);
        gameObject.layer = DarkActivation
            ? LayerMask.NameToLayer("Enabled")
            : LayerMask.NameToLayer("Disabled");
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (ActivationPattern.Count <= 0 || _lightCount == 0) return;
        if (hasFocus)
        {
            _activationIndex = (_activationIndex + 1) % ActivationPattern.Count;
            Visual.SetActive(ActivationPattern[_activationIndex]);
            gameObject.layer = ActivationPattern[_activationIndex]
                ? LayerMask.NameToLayer("Enabled")
                : LayerMask.NameToLayer("Disabled");
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (ActivationPattern.Count <= 0) return;
        if (!other.CompareTag("Light")) return;
        _lightCount += 1;
        if (_lightCount != 1) return;

        Visual.SetActive(ActivationPattern[_activationIndex]);
        gameObject.layer = ActivationPattern[_activationIndex]
            ? LayerMask.NameToLayer("Enabled")
            : LayerMask.NameToLayer("Disabled");
        _enabledFrame = ActivationPattern[_activationIndex];

        if (!ActivationPattern[_activationIndex]) return;
        if (CompareTag("DoorEnter")) return;

        Collider2D[] cols = new Collider2D[1];
        var filter = new ContactFilter2D();
        filter.SetLayerMask(LayerMask.GetMask("Character"));
        GetComponent<Collider2D>().OverlapCollider(filter, cols);
        if (cols[0])
        {
            Visual.SetActive(false);
            gameObject.layer = LayerMask.NameToLayer("Disabled");
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (ActivationPattern.Count <= 0) return;
        if (!other.CompareTag("Light")) return;
        _lightCount -= 1;
        if (_lightCount != 0) return;
        _activationIndex = (_activationIndex + 1) % ActivationPattern.Count;
        Visual.SetActive(DarkActivation);
        gameObject.layer = DarkActivation
            ? LayerMask.NameToLayer("Enabled")
            : LayerMask.NameToLayer("Disabled");

        if (!DarkActivation) return;
        if (CompareTag("DoorEnter")) return;

        Collider2D[] cols = new Collider2D[1];
        var filter = new ContactFilter2D();
        filter.SetLayerMask(LayerMask.GetMask("Character"));
        GetComponent<Collider2D>().OverlapCollider(filter, cols);
        if (cols[0])
        {
            Visual.SetActive(false);
            gameObject.layer = LayerMask.NameToLayer("Disabled");
        }
    }

    private void Update()
    {
        _enabledFrame = false;
    }
}
