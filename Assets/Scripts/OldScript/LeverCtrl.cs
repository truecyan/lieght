﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverCtrl : MonoBehaviour
{
    public LampCtrlOld Lamp;
    public List<GameObject> Linked;
    public bool On = true;

    // Start is called before the first frame update
    void Start()
    {
        SetState(On);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetState(bool state)
    {
        var s = transform.localScale;
        transform.localScale = state ? new Vector3(s.x, 1, s.z) : new Vector3(s.x, -1, s.z);
        Lamp.SetLight(state);
        On = state;
        foreach (var link in Linked)
        {
            link.SetActive(state);
        }
    }
    
}
