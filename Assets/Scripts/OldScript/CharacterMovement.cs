﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public ColCheck Feet;
    public float MoveSpeed = 4f;
    public float JumpForce = 7f;
    public float ShootForce = 10f;
    private Rigidbody2D rb2D;
    private Collider2D col2D;
    //private Animator anim;
    //private SpriteRenderer sprite;
    public bool isLanding;

    private bool _doInteract;

    public List<LeverCtrl> LeverList;
    public List<DoorCtrl> DoorList;
    public List<GameObject> LadderList;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
        //anim = gameObject.GetComponent<Animator>();
        //sprite = gameObject.GetComponent<SpriteRenderer>();
    }
    void FixedUpdate()
    {
        rb2D.velocity = new Vector2(Input.GetAxis("Horizontal") * MoveSpeed, rb2D.velocity.y);

        if (isLanding || LadderList.Count>0)
        {
            if (Input.GetButton("Jump"))
            {
                rb2D.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
                isLanding = false;
            }
        }

        if (LadderList.Count > 0)
        {
            rb2D.gravityScale = 0;
            rb2D.velocity = new Vector2(rb2D.velocity.x, Input.GetAxis("Vertical") * MoveSpeed);
        }
        else rb2D.gravityScale = 1;

        if (_doInteract)
        {
            _doInteract = false;
            var lever = LeverList[0];
            lever.SetState(!lever.On);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log(other.tag);
        if (!CompareTag("Character")) return;
        if (other.CompareTag("Lever"))
        {
            var lever = other.GetComponent<LeverCtrl>();
            if (LeverList.Count == 0)
            {
                LeverList.Add(lever);
                return;
            }

            var y = other.transform.position.y;
            for (int i = 0; i < LeverList.Count; i++)
            {
                if (LeverList[i].transform.position.y < y)
                {
                    LeverList.Insert(i, lever);
                    break;
                }
            }
        }
        else if (other.CompareTag("Door"))
        {
            var door = other.GetComponent<DoorCtrl>();
            if (DoorList.Count == 0)
            {
                DoorList.Add(door);
                return;
            }

            var y = other.transform.position.y;
            for (int i = 0; i < DoorList.Count; i++)
            {
                if (DoorList[i].transform.position.y < y)
                {
                    DoorList.Insert(i, door);
                    break;
                }
            }
        }
        else if (other.CompareTag("Ladder"))
        {
            var ladder = other.gameObject;
            LadderList.Add(ladder);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!CompareTag("Character")) return;
        if (other.CompareTag("Lever")) LeverList.Remove(other.GetComponent<LeverCtrl>());
        else if (other.CompareTag("Door")) DoorList.Remove(other.GetComponent<DoorCtrl>());
        else if (other.CompareTag("Ladder")) LadderList.Remove(other.gameObject);
    }
    private void OnCollisionStay2D(Collision2D other)
    {
        if (Feet.IsColliding())
            isLanding = true;
    }
    private void OnCollisionExit2D(Collision2D other)
    {
        if (!Feet.IsColliding())
            isLanding = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(rb2D.velocity);
        //anim.SetBool("isLanding", isLanding);
        //anim.SetBool("isFalling", rb2D.velocity.y <= 0);
        
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.001f)
        {
            //anim.SetBool("isWalking", true);
            transform.localScale = new Vector3(Input.GetAxis("Horizontal") < 0?-1:1,1);
        }
        //else anim.SetBool("isWalking", false);

        if (Input.GetButtonDown("Interaction"))
        {
            if (LeverList.Count > 0) _doInteract = true;
            if (DoorList.Count > 0)
            {
                var door = DoorList[0];
                Initiate.Fade(door.Destination, Color.black, 1.0f);
            }
        }

        if (Input.GetButtonDown("Shoot"))
        {
            var dir = transform.localScale.x;
            var ray = Physics2D.Raycast(transform.position + new Vector3(0.5f, 0f) * dir, Vector2.right * dir);
            
            if (ray)
            {
                if (ray.transform.CompareTag("Lamp"))
                {
                    var lamp = ray.transform.GetComponent<LampActivation>().Lamp;
                    lamp.Shot(ShootForce);
                }
            }
        }
    }
}
