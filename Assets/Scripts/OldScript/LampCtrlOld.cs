﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class LampCtrlOld : MonoBehaviour
{
    public bool Activated = true;
    public bool On = true;
    public GameObject Light;
    public GameObject Body;
    public GameObject RopePrefab;
    private GameObject _rope;
    private float _omega;
    private float _length;
    public float Amplitude;
    private float _phase;

    // Start is called before the first frame update
    void Start()
    {
        SetLight(On);
        Hang();
    }

    public void Hang()
    {
        var ray = Physics2D.Raycast(transform.position+new Vector3(0,0.5f), Vector2.up, LayerMask.GetMask("Enabled"));
        if (ray)
        {
            _rope = Instantiate(RopePrefab, transform);
            _rope.transform.localScale = new Vector3(1,ray.distance+0.5f,1);
            var bodyPos = Body.transform.position;
            transform.position = ray.point;
            Body.transform.position = bodyPos;
            _rope.transform.position = ray.point;
            _length = (ray.distance + 1);
            _omega = Mathf.Sqrt((-Physics2D.gravity.y/2)/(ray.distance + 1));
            _phase = 0;
        }
    }

    public void DestroyRope()
    {
        Destroy(_rope);
        _rope = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (_rope && Amplitude > 0)
        {
            _phase = (_phase+_omega*Time.deltaTime)%(2*Mathf.PI);
            transform.rotation = Quaternion.Euler(0,0,Amplitude*Mathf.Sin(_phase));
        }
    }

    public void SetLight(bool state)
    {
        On = state;
        Light.SetActive(state);
    }

    public void SetActivation(bool state)
    {
        if (!Activated)
        {
            On = false;
            Light.SetActive(false);
        }
        Activated = state;
    }

    public void Shot(float force)
    {
        var prevAmp = Amplitude;
        Amplitude = Mathf.Sqrt(Sqr(Amplitude) + Sqr(force * _length * Mathf.Cos(_phase)));
        if (Amplitude > 90) Amplitude = 90;
        _phase = Mathf.Asin(prevAmp / Amplitude * Mathf.Sin(_phase));
    }

    private float Sqr(float a)
    {
        return a * a;
    }
}
