﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scripter : MonoBehaviour
{
    public RectTransform Panel;
    public Text Text;
    public float WordWidth = 48;
    public float ExpandPeriod = 0.2f;
    public float WaitTime = 1f;
    [Multiline]
    public string Script;

    private string[] _lineScript;
    private int _currLine;
    private bool _expand = true;
    private bool _end;
    // Start is called before the first frame update
    void Start()
    {
        _lineScript = Script.Split('\n');
        Debug.Log(_lineScript.Length);
        Panel.sizeDelta = new Vector2(WordWidth, Panel.rect.height);
        //Text.text = _lineScript[_currLine];
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus)
        {
            gameObject.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (_expand)
        {
            var nextWidth = Panel.rect.width + WordWidth * (1 / ExpandPeriod) * Time.deltaTime;
            if (nextWidth < WordWidth * _lineScript[_currLine].Length)
            {
                Panel.sizeDelta = new Vector2(nextWidth, Panel.rect.height);
                Text.text = _lineScript[_currLine].Substring(0, Mathf.FloorToInt(nextWidth / WordWidth));
            }
            else
            {
                Panel.sizeDelta = new Vector2(WordWidth * _lineScript[_currLine].Length, Panel.rect.height);
                Text.text = _lineScript[_currLine];
                _expand = false;
                StartCoroutine(Wait());
            }
        }

        if (Input.GetButtonDown("Submit"))
        {
            if (_expand)
            {
                Panel.sizeDelta = new Vector2(WordWidth * _lineScript[_currLine].Length, Panel.rect.height);
                Text.text = _lineScript[_currLine];
                _expand = false;
                if (_currLine == _lineScript.Length - 1) _end = true;
                else StartCoroutine(Wait());
            }
            else
            {
                Skip();
            }
        }
    }

    private void StartNext()
    {
        Panel.sizeDelta = new Vector2(WordWidth, Panel.rect.height);
        _expand = true;
    }
    public void Skip()
    {
        if (_currLine < _lineScript.Length-1)
        {
            _currLine += 1;
            Debug.Log(_currLine);
            StartNext();
        }
        else if(!_end)
        {
            _end = true;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
    private IEnumerator Wait()
    {
        _end = true;
        yield return new WaitForSeconds(WaitTime);
        _end = false;
        Skip();
    }
}
