﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CameraMovement))]
public class CameraEditor : Editor
{
    void OnSceneGUI()
    {
        var zone = (CameraMovement)target;

        Handles.DrawSolidRectangleWithOutline(new Rect(zone.LimitB, zone.LimitA - zone.LimitB), Color.clear, Color.cyan);
        Handles.color = Color.cyan;

        var handlesPos = new[] {
            new Vector3(zone.LimitA.x, (zone.LimitA.y+zone.LimitB.y)/2, 0),
            new Vector3(zone.LimitB.x, (zone.LimitA.y+zone.LimitB.y)/2, 0),
            new Vector3((zone.LimitA.x+zone.LimitB.x)/2, zone.LimitA.y, 0),
            new Vector3((zone.LimitA.x+zone.LimitB.x)/2, zone.LimitB.y, 0)
        };

        float size = HandleUtility.GetHandleSize(zone.transform.position) * 0.1f;
        Vector3 snap = Vector3.one * 0.5f;

        EditorGUI.BeginChangeCheck();
        Vector3 hPos0 = Handles.FreeMoveHandle(handlesPos[0], Quaternion.identity, size, snap, Handles.CubeHandleCap);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(zone, "Changed Scale");
            zone.LimitA = new Vector2(hPos0.x, zone.LimitA.y);
        }

        EditorGUI.BeginChangeCheck();
        Vector3 hPos1 = Handles.FreeMoveHandle(handlesPos[1], Quaternion.identity, size, snap, Handles.CubeHandleCap);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(zone, "Changed Scale");
            zone.LimitB = new Vector2(hPos1.x, zone.LimitB.y);

        }
        EditorGUI.BeginChangeCheck();
        Vector3 hPos2 = Handles.FreeMoveHandle(handlesPos[2], Quaternion.identity, size, snap, Handles.CubeHandleCap);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(zone, "Changed Scale");
            zone.LimitA = new Vector2(zone.LimitA.x, hPos2.y);
        }
        EditorGUI.BeginChangeCheck();
        Vector3 hPos3 = Handles.FreeMoveHandle(handlesPos[3], Quaternion.identity, size, snap, Handles.CubeHandleCap);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(zone, "Changed Scale");
            zone.LimitB = new Vector2(zone.LimitB.x, hPos3.y);
        }


    }
}
