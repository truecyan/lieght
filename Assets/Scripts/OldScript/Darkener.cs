﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Darkener : MonoBehaviour
{
    public GameObject DarkPanel;
    public List<GameObject> TextBoxGroupList;
    private int _currIndex;

    private void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus)
        {
            if (TextBoxGroupList.Count > 0)
            {
                TextBoxGroupList[_currIndex].SetActive(false);
                _currIndex = (_currIndex + 1) % TextBoxGroupList.Count;
                TextBoxGroupList[_currIndex].SetActive(true);
            }
        }
        DarkPanel.SetActive(!hasFocus);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
