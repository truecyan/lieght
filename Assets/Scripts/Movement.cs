﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    public class KeyInfo
    {
        public float down;
        public float up;

        public KeyInfo()
        {
            down = -1;
            up = -1;
        }
    }
    public class KeyMap
    {
        public KeyInfo jump;
        public KeyInfo sneak;
        public KeyInfo interaction;
        public KeyInfo vertical;
        public KeyInfo horizontal;

        public KeyMap()
        {
            jump = new KeyInfo();
            sneak = new KeyInfo();
            interaction = new KeyInfo();
            vertical = new KeyInfo();
            horizontal = new KeyInfo();
        }
    }

    public float JumpHeight = 2.2f;
    public float JumpLength = 6f;
    public float JumpTime = 2f;
    public float InputBuffer = 0.1f;
    public float NegativeYLimit = -10;

    private PlatformerPhysics _pp;
    private float _velX;
    private float _velY;
    private KeyMap _keyMap = new KeyMap();
    private AnimCtrl _ac;
    private NearRecog _nr;
    private List<SwitchCtrl> _switchList = new List<SwitchCtrl>();
    private List<DoorCtrl> _doorList = new List<DoorCtrl>();
    private bool _fading;
    KeyInfo GetKeyInfo(string keyName)
    {
        switch (keyName)
        {
            case "Jump":
                return _keyMap.jump;
            case "Sneak":
                return _keyMap.sneak;
            case "Interaction":
                return _keyMap.interaction;
            case "Vertical":
                return _keyMap.vertical;
            case "Horizontal":
                return _keyMap.horizontal;
            default:
                return null;
        }
    }

    bool UseKeyDown(string keyName)
    {
        var key = GetKeyInfo(keyName);

        if ((Time.time - key.down) < InputBuffer)
        {
            key.down = -1;
            return true;
        }
        return false;
    }
    bool UseKeyDownBufferFree(string keyName)
    {
        var key = GetKeyInfo(keyName);

        if (key.down > 0)
        {
            key.down = -1;
            return true;
        }
        return false;
    }
    bool UseKeyUp(string keyName)
    {
        var key = GetKeyInfo(keyName);

        if ((Time.time - key.up) < InputBuffer)
        {
            key.up = -1;
            return true;
        }
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        _pp = GetComponent<PlatformerPhysics>();
        _ac = GetComponent<AnimCtrl>();
        _nr = GetComponent<NearRecog>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!CompareTag("Character")) return;
        if (other.CompareTag("Switch"))
        {
            _switchList.Add(other.GetComponent<SwitchCtrl>());
        }
        if (other.CompareTag("Door"))
        {
            _doorList.Add(other.GetComponent<DoorCtrl>());
        }

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (!CompareTag("Character")) return;
        if (other.CompareTag("Switch"))
        {
            _switchList.Remove(other.GetComponent<SwitchCtrl>());
        }
        if (other.CompareTag("Door"))
        {
            _doorList.Remove(other.GetComponent<DoorCtrl>());
        }
    }
    //모든 이동은 FixedUpdate에서
    //모든 인풋은 Update에서
    void FixedUpdate()
    {

        var vel = _pp.GetVel();
        _velX = vel.x;
        _velY = vel.y;
        var endVel = 4/JumpTime*JumpHeight;
        if (UseKeyDown("Vertical"))
        {
            _pp.TryHold(Input.GetAxis("Vertical") > 0);
            var newVel = _pp.GetVel();
            _velX = newVel.x;
            _velY = newVel.y;
        }
        if (_pp.IsLanded() && UseKeyDownBufferFree("Horizontal"))
        {
            _pp.EndHold();
        }
        //좌우 이동
        if (_pp.IsHolding())
        {
            _velY = Input.GetAxis("Vertical") * JumpLength / JumpTime;
        }
        else
        {
            _velX = Input.GetAxis("Horizontal") * JumpLength / JumpTime;
        }

        
        if (UseKeyDown("Sneak") && (!_pp.IsHolding()||_pp.IsLanded()))
        {
            _ac.Sneak();
            _pp.SetSneak(true);
            _nr.SmallRecog(true);
        }
        if ((_pp.IsHolding() && _pp.IsSneaked()) || UseKeyUp("Sneak"))
        {
            _ac.Stand();
            _pp.SetSneak(false);
            _nr.SmallRecog(false);
        }
        if (UseKeyDown("Jump"))
        {
            if (_pp.IsLanded())
            {
                _pp.Jump();
                if (_pp.IsSneaked()) _velY = endVel / 4;
                else _velY = endVel;
            }
            _pp.EndHold();
        }
        if (_velY > 0 && UseKeyUp("Jump"))
        {
            _velY /= 2;
        }
        if (!_pp.IsLanded() && !_pp.IsHolding())
        {
            if (_velY > -endVel)
                _velY -= 8*JumpHeight/Sqr(JumpTime)*Time.deltaTime;
            else _velY = -endVel;
        }

        if (_doorList.Count > 0 && UseKeyDown("Interaction"))
        {
            var x = transform.position.x;
            var minDist = Mathf.Abs(x - _doorList[0].transform.position.x);
            var nearest = _doorList[0];
            for (var i = 1; i < _doorList.Count; i++)
            {
                var s = _doorList[i];
                var dist = Mathf.Abs(x - s.transform.position.x);
                if (minDist > dist)
                {
                    minDist = dist;
                    nearest = s;
                }
            }
            Initiate.Fade(nearest.Destination, Color.black, 1.0f);
        }

        if (_switchList.Count > 0 && UseKeyDown("Interaction"))
        {
            var x = transform.position.x;
            var minDist = Mathf.Abs(x-_switchList[0].transform.position.x);
            var nearest = _switchList[0];
            for(var i = 1; i < _switchList.Count; i++)
            {
                var s = _switchList[i];
                var dist = Mathf.Abs(x - s.transform.position.x);
                if (minDist > dist)
                {
                    minDist = dist;
                    nearest = s;
                }
            }
            nearest.InvertState();
        }
        
        _pp.SetVel(_velX,_velY); //PlatformerPhysics는 Movement 보다 나중에 실행됨
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            _keyMap.jump.down = Time.time;
        }
        if (Input.GetButtonUp("Jump"))
        {
            _keyMap.jump.up = Time.time;
        }
        if (Input.GetButtonDown("Sneak"))
        {
            _keyMap.sneak.down = Time.time;
        }
        if (Input.GetButtonUp("Sneak"))
        {
            _keyMap.sneak.up = Time.time;
        }
        if (Input.GetButtonDown("Interaction"))
        {
            _keyMap.interaction.down = Time.time;
        }
        if (Input.GetButtonUp("Interaction"))
        {
            _keyMap.interaction.up = Time.time;
        }
        if (Input.GetButtonDown("Vertical"))
        {
            _keyMap.vertical.down = Time.time;
        }
        if (Input.GetButtonUp("Vertical"))
        {
            _keyMap.vertical.up = Time.time;
        }
        if (Input.GetButtonDown("Horizontal"))
        {
            _keyMap.horizontal.down = Time.time;
        }
        if (Input.GetButtonUp("Horizontal"))
        {
            _keyMap.horizontal.up = Time.time;
        }
    }

    public float MaxVelX()
    {
        return JumpLength / JumpTime;
    }
    public float Sqr(float a)
    {
        return a * a;
    }
}
