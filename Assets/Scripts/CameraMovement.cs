﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraMovement : MonoBehaviour
{
    public Vector2 LimitA;
    public Vector2 LimitB;
    public GameObject Character;

    public bool AutoScroll;
    public float ScrollSpeed;

    public float CamSize = 8;
    public Camera LightCamera;
    public Camera DarkCamera;

    private bool _fading;
    

    // Start is called before the first frame update
    void Start()
    {
        LightCamera.orthographicSize = CamSize;
        DarkCamera.orthographicSize = CamSize;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 pos = transform.position;
        Vector2 cPos = Character.transform.position;
        if (!LightCamera.pixelRect.Contains(LightCamera.WorldToScreenPoint(cPos)) && !_fading)
        {
            Initiate.Fade(SceneManager.GetActiveScene().name, Color.black, 2f);
            _fading = true;
        }

        if (AutoScroll)
        {
            var newPos = pos + new Vector2(ScrollSpeed, 0) * Time.fixedDeltaTime;
            var deltaA = newPos - LimitA;
            var deltaB = newPos - LimitB;
            if (deltaA.x * deltaB.x <= 0)
            {
                transform.position = newPos;
            }
        }
        else
        {
            var deltaA = cPos - LimitA;
            var deltaB = cPos - LimitB;
            if (deltaA.x * deltaB.x <= 0)
            {
                transform.position = new Vector3(cPos.x, pos.y);
            }
            if (deltaA.y * deltaB.y <= 0)
            {
                transform.position = new Vector3(pos.x, cPos.y);
            }
        }
    }
}
