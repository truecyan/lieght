﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro.EditorUtilities;
using UnityEngine;

public class PlatformerPhysics : MonoBehaviour
{
    public float StandHeight = 1.5f;
    public float SneakHeight = 0.75f;
    [SerializeField] private float _velX;
    [SerializeField] private float _velY;
    [SerializeField] private bool _landed;
    [SerializeField] private bool _blocked;
    [SerializeField] private bool _holding;
    private bool _sneaked;
    private bool _jumped;
    private bool _onOneSide;

    private const float CharWidth = 0.75f;
    private const float Offset = 0.01f;
    private readonly Vector2 _feetPos = new Vector2(0, -0.5f);
    private readonly Vector2 _midPos = new Vector2(0, 0.25f);
    private readonly Vector2 _headPos = new Vector2(0, 1f);

    private BoxCollider2D _col;
    private float _maxVelX;
    private bool _ladderPositive;

    // Start is called before the first frame update
    void Start()
    {
        _col = GetComponent<BoxCollider2D>();
        _maxVelX = GetComponent<Movement>().MaxVelX();
    }
    //리지드바디 사용 금지
    void FixedUpdate()
    {
        CheckVert();//바닥/천장 충돌 처리

        if (Mathf.Abs(_velX) > Offset)
        {
            CheckHor();//벽 충돌 처리
        }

        if (!_landed && !_blocked) 
        {
            CheckDiag();//모서리 충돌 처리
        }

        //이동 적용
        if (_holding)
        {
            ValidateHold();
        }
        transform.Translate(GetVel() * Time.fixedDeltaTime);
        //Debug.Log(GetVel());
    }

    public void CheckVert()
    {
        Vector2 pos = transform.position;
        var physicsLayer = LayerMask.GetMask("Physics");
        if (_velY <= 0)
        {
            var feetRayPos = pos + _feetPos + Vector2.up * Offset;
            var checkDist = -_velY * Time.fixedDeltaTime + Offset;
            var downBox = Physics2D.OverlapBoxAll(feetRayPos,
                new Vector2(CharWidth - Offset * 2, checkDist * 2), 0, physicsLayer);
            Physics2D.queriesHitTriggers = true;
            var downTriggerBox = Physics2D.OverlapBoxAll(feetRayPos,
                new Vector2(CharWidth - Offset * 2, checkDist * 2), 0, physicsLayer);
            Physics2D.queriesHitTriggers = false;

            if (downBox.Length + downTriggerBox.Length > 0)
            {
                var minDist = checkDist;
                foreach (var col in downBox)
                {
                    var colDist =_col.Distance(col);
                    var distance = colDist.distance;
                    if (distance < minDist) minDist = distance;
                }

                var onesideExists = false;
                if (!(_sneaked && !_landed && _jumped) && !_holding)
                {
                    foreach (var col in downTriggerBox)
                    {
                        if (col.CompareTag("OnesideFloor"))
                        {
                            onesideExists = true;
                            var colDist = _col.Distance(col);
                            var distance = colDist.distance;
                            if (distance < minDist) minDist = distance;
                        }
                    }
                }
                

                if (downBox.Length > 0 || onesideExists)
                {
                    transform.Translate(minDist * Vector2.down);
                    _velY = 0;
                    _landed = true;
                    _jumped = false;
                }
                else
                {
                    _landed = false;
                }
            }
            else _landed = false;
        }
        else
        {
            _landed = false;

            var headRayPos = pos + _headPos + Vector2.down * Offset;
            var checkDist = _velY * Time.fixedDeltaTime + Offset;
            if (_sneaked) headRayPos = pos + _midPos + Vector2.down * Offset;

            var upBox = Physics2D.OverlapBoxAll(headRayPos,
                new Vector2(CharWidth - Offset * 2, checkDist * 2), 0, physicsLayer);

            if (upBox.Length > 0)
            {
                var minDist = checkDist;
                foreach (var col in upBox)
                {
                    var colDist = _col.Distance(col);
                    var distance = colDist.distance;
                    if (distance < minDist) minDist = distance;
                }

                transform.Translate(minDist * Vector2.up);
                //Debug.Log(transform.position.y.ToString());
                _velY = 0;
            }
        }
    }
    public void CheckHor()
    {
        Vector2 pos = transform.position;
        var physicsLayer = LayerMask.GetMask("Physics");
        var hor = new Vector2(CharWidth / 2 - Offset, 0) * Mathf.Sign(_velX);
        var dir = Vector2.right * Mathf.Sign(_velX);
        var checkDist = Mathf.Abs(_velX) * Time.fixedDeltaTime + Offset;

        if (_sneaked)
        {
            var horSmallBox = Physics2D.OverlapBoxAll(pos + (_midPos + _feetPos) / 2 + hor,
                new Vector2(checkDist * 2, (_midPos - _feetPos).y - Offset*2), 0, physicsLayer);
            if (horSmallBox.Length > 0)
            {
                var minDist = checkDist;
                foreach (var col in horSmallBox)
                {
                    var colDist = _col.Distance(col);
                    var distance = colDist.distance;
                    if (distance < minDist) minDist = distance;
                }

                transform.Translate(minDist * dir);
                _blocked = true;
                _velX = 0;
            }
            else _blocked = false;

            return;
        }
        var horBox = Physics2D.OverlapBoxAll(pos + (_headPos + _feetPos) / 2 + hor,
            new Vector2(checkDist * 2, (_headPos - _feetPos).y - Offset * 2), 0, physicsLayer);
        if (horBox.Length > 0)
        {
            var minDist = checkDist;
            foreach (var col in horBox)
            {
                var colDist = _col.Distance(col);
                var distance = colDist.distance;
                if (distance < minDist) minDist = distance;
            }
            transform.Translate(minDist * dir);
            _blocked = true;
            _velX = 0;
        }
        else _blocked = false;
    }

    public void CheckDiag()
    {
        Vector2 pos = transform.position;
        var physicsLayer = LayerMask.GetMask("Physics");
        var hor = new Vector2(CharWidth / 2, 0) * Mathf.Sign(_velX);
        var checkPos = pos + _feetPos; //내려갈 땐 발쪽이다
        if (_velY > 0) {
            if(_sneaked) checkPos = pos + _midPos;
            else checkPos = pos + _headPos; //올라갈 땐 머리쪽이다
        }
        var dir = GetVel().normalized;

        var checkRayPos = checkPos + hor - dir * Offset;
        var checkRay = Physics2D.Raycast(checkRayPos, dir, GetVel().magnitude * Time.fixedDeltaTime + Offset, physicsLayer);
        if (checkRay)
        {
            //Debug.Log("Yeah");
            var moveVec = (checkRay.distance - Offset) * dir;

            var norm = checkRay.normal; //법선 방향으로 예상 충돌 지점이 벽/바닥인지 판정
            if (Mathf.Abs(norm.x) < Offset) //벽쪽
            {
                transform.Translate(new Vector2(0, moveVec.y));
                _landed = (_velY <= 0);
                _velY = 0;
            }
            else //바닥쪽
            {
                transform.Translate(new Vector2(moveVec.x, 0));
                _velX = 0;
                _blocked = true;
            }
        }
    }
    public void SetVel(float x, float y)
    {
        _velX = x;
        _velY = y;
    }
    public Vector2 GetVel()
    {
        return new Vector2(_velX, _velY);
    }

    public bool IsLanded()
    {
        return _landed;
    }
    public bool IsBlocked()
    {
        return _blocked;
    }

    public bool IsSneaked()
    {
        return _sneaked;
    }

    public void SetSneak(bool sneak)
    {
        _sneaked = sneak;
        _jumped = false;
        var newHeight = StandHeight;
        if (sneak)
        {
            newHeight = SneakHeight;
        }
        _col.size = new Vector2(_col.size.x, newHeight);
        _col.offset = new Vector2(_col.offset.x, newHeight / 2 - 0.5f);
    }

    public void TryHold(bool toUp)
    {
        if (!_landed) return;
        Vector2 pos = transform.position;
        var physicsLayer = LayerMask.GetMask("Physics");
        var feetRayPos = pos + _feetPos + Vector2.up * Offset;
        var dir = Mathf.Sign(Input.GetAxis("Vertical"));
        Physics2D.queriesHitTriggers = true;
        var checkBox = Physics2D.OverlapBoxAll(feetRayPos + dir * Offset * Vector2.up,
            new Vector2(CharWidth - Offset * 2, Offset), 0, physicsLayer);
        Physics2D.queriesHitTriggers = false;
        foreach (var elem in checkBox)
        {
            if (elem.CompareTag("Ladder"))
            {
                if (elem.transform.position.y > feetRayPos.y != toUp) continue;
                _holding = true;

                var delta = (transform.position.x + _velX * Time.fixedDeltaTime - elem.transform.position.x);
                _ladderPositive = delta * _velX >= 0;
                if (_velX < _maxVelX && _velX > 0) _velX = _maxVelX;
                if (_velX > -_maxVelX && _velX < 0) _velX = -_maxVelX;

                if (_ladderPositive)
                {
                    _velX = -Mathf.Sign(delta) * _maxVelX;
                }
            }
        }
    }

    public void ValidateHold()
    {
        Vector2 pos = transform.position;
        var physicsLayer = LayerMask.GetMask("Physics");
        var feetRayPos = pos + _feetPos + Vector2.up * Offset;
        var dir = Mathf.Sign(Input.GetAxis("Vertical"));
        Physics2D.queriesHitTriggers = true;
        var checkBox = Physics2D.OverlapBoxAll(feetRayPos + dir * Offset * Vector2.up,
            new Vector2(CharWidth - Offset * 2, Offset), 0, physicsLayer);
        Physics2D.queriesHitTriggers = false;
        _holding = false;
        foreach (var elem in checkBox)
        {
            if (elem.CompareTag("Ladder"))
            {
                _holding = true;
                if (((transform.position.x + _velX * Time.fixedDeltaTime - elem.transform.position.x) * _velX >= 0))
                {
                    transform.position = new Vector3(elem.transform.position.x, pos.y, 0);
                    _velX = 0;
                }
            }
        }
    }

    public void Jump()
    {
        _jumped = true;
    }
    public bool IsJumped()
    {
        return _jumped;
    }
    public void EndHold()
    {
        _holding = false;
    }
    public bool IsHolding()
    {
        return _holding;
    }
}
