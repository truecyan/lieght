﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LightMap))]
public class LampCtrl : MonoBehaviour
{
    public string LampSymbol;
    public bool DefaultOn = true;
    public GameObject DarkSprite;

    private LightMap _lm;
    private bool _state;

    // Start is called before the first frame update
    void Start()
    {
        LightManager.Instance.SwitchUpdateEvent += OnSwitchUpdate;
        _lm = GetComponent<LightMap>();
        if (DefaultOn)
        {
            _lm.LightOn();
            DarkSprite.SetActive(true);
        }
        else
        {
            _lm.LightOff();
            DarkSprite.SetActive(false);
        }
        _state = DefaultOn;
    }

    void OnSwitchUpdate(string symbol, bool state)
    {
        if (symbol.Equals(LampSymbol) && state != _state)
        {
            if (state)
            {
                _lm.LightOn();
                DarkSprite.SetActive(true);
            }
            else
            {
                _lm.LightOff();
                DarkSprite.SetActive(false);
            }
            _state = state;
        }
    }
}
