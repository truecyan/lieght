﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BoxColVert : ColVert
{
    private BoxCollider2D _col;
    // Start is called before the first frame update
    void Start()
    {
        _col = GetComponent<BoxCollider2D>();
    }

    public override Vector2[] GetVertices()
    {
        Vector2 extents = _col.size / 2;
        Vector2 scale = transform.localScale;
        extents = new Vector2(extents.x * scale.x, extents.y * scale.y);
        Vector2[] vertices = new Vector2[4];
        Vector2 pos = _col.transform.position;
        Vector2 up = _col.transform.up * extents.y;
        Vector2 right = _col.transform.right * extents.x;
        vertices[0] = pos + up - right;
        vertices[1] = pos + up + right;
        vertices[2] = pos - up + right;
        vertices[3] = pos - up - right;
        return vertices;
    }
}
