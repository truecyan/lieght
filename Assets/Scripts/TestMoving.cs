﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMoving : MonoBehaviour
{
    private Collider2D _col;
    // Start is called before the first frame update
    void Start()
    {
        _col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(new Vector3(Input.GetAxis("Horizontal") * 4f, Input.GetAxis("Vertical") * 4f) * Time.fixedDeltaTime);
    }
}
