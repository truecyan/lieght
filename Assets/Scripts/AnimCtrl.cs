﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimCtrl : MonoBehaviour
{
    public SpriteRenderer CharSpr;
    public SpriteRenderer DarkCharSpr;
    public Sprite StandSpr;
    public Sprite SneakSpr;
    public Sprite DarkStandSpr;
    public Sprite DarkSneakSpr;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Sneak()
    {
        CharSpr.sprite = SneakSpr;
        DarkCharSpr.sprite = DarkSneakSpr;
    }
    public void Stand()
    {
        CharSpr.sprite = StandSpr;
        DarkCharSpr.sprite = DarkStandSpr;
    }
}
