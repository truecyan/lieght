﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StylishDebug : MonoBehaviour
{
    public static void DrawSquare(Vector3 point, float size, Color color)
    {
        Debug.DrawLine(point + new Vector3(size / 2, size / 2), point + new Vector3(size / 2, -size / 2), color);
        Debug.DrawLine(point + new Vector3(size / 2, -size / 2), point + new Vector3(-size / 2, -size / 2), color);
        Debug.DrawLine(point + new Vector3(-size / 2, -size / 2), point + new Vector3(-size / 2, size / 2), color);
        Debug.DrawLine(point + new Vector3(-size / 2, size / 2), point + new Vector3(size / 2, size / 2), color);
    }
    public static void DrawSquare(Vector3 point, float size)
    {
        Debug.DrawLine(point + new Vector3(size / 2, size / 2), point + new Vector3(size / 2, -size / 2));
        Debug.DrawLine(point + new Vector3(size / 2, -size / 2), point + new Vector3(-size / 2, -size / 2));
        Debug.DrawLine(point + new Vector3(-size / 2, -size / 2), point + new Vector3(-size / 2, size / 2));
        Debug.DrawLine(point + new Vector3(-size / 2, size / 2), point + new Vector3(size / 2, size / 2));
    }
    public static void DrawDiamond(Vector3 point, float size, Color color)
    {
        Debug.DrawLine(point + new Vector3(-size / 2, 0), point + new Vector3(0, size / 2), color);
        Debug.DrawLine(point + new Vector3(0, size / 2), point + new Vector3(size / 2, 0), color);
        Debug.DrawLine(point + new Vector3(size / 2, 0), point + new Vector3(0, -size / 2), color);
        Debug.DrawLine(point + new Vector3(0, -size / 2), point + new Vector3(-size / 2, 0), color);
    }
    public static void DrawDiamond(Vector3 point, float size)
    {
        Debug.DrawLine(point + new Vector3(-size / 2, 0), point + new Vector3(0, size / 2));
        Debug.DrawLine(point + new Vector3(0, size / 2), point + new Vector3(size / 2, 0));
        Debug.DrawLine(point + new Vector3(size / 2, 0), point + new Vector3(0, -size / 2));
        Debug.DrawLine(point + new Vector3(0, -size / 2), point + new Vector3(-size / 2, 0));
    }
    public static void DrawCross(Vector3 point, float size, Color color)
    {
        Debug.DrawLine(point + new Vector3(size / 2, size / 2), point + new Vector3(-size / 2, -size / 2), color);
        Debug.DrawLine(point + new Vector3(size / 2, -size / 2), point + new Vector3(-size / 2, size / 2), color);
    }
    public static void DrawCross(Vector3 point, float size)
    {
        Debug.DrawLine(point + new Vector3(size / 2, size / 2), point + new Vector3(-size / 2, -size / 2));
        Debug.DrawLine(point + new Vector3(size / 2, -size / 2), point + new Vector3(-size / 2, size / 2));
    }
    public static void DrawPlus(Vector3 point, float size, Color color)
    {
        Debug.DrawLine(point + new Vector3(size / 2, 0), point + new Vector3(-size / 2, 0), color);
        Debug.DrawLine(point + new Vector3(0, size / 2), point + new Vector3(0, -size / 2), color);
    }
    public static void DrawPlus(Vector3 point, float size)
    {
        Debug.DrawLine(point + new Vector3(size / 2, 0), point + new Vector3(-size / 2, 0));
        Debug.DrawLine(point + new Vector3(0, size / 2), point + new Vector3(0, -size / 2));
    }
    public static void DrawArrow(Vector3 start, Vector3 end, float headSize, Color color)
    {
        Debug.DrawLine(start, end, color);
        var orthogonal = Vector3.Cross(end - start, Vector3.forward).normalized * headSize / 2;
        var back = -(end - start).normalized * headSize / 2;
        Debug.DrawLine(end, end - orthogonal + back, color);
        Debug.DrawLine(end, end + orthogonal + back, color);
    }
    public static void DrawArrow(Vector3 start, Vector3 end, float headSize)
    {
        Debug.DrawLine(start, end);
        var orthogonal = Vector3.Cross(end - start, Vector3.forward).normalized * headSize / 2;
        var back = -(end - start).normalized * headSize / 2;
        Debug.DrawLine(end, end - orthogonal + back);
        Debug.DrawLine(end, end + orthogonal + back);
    }
    public static void DrawArrow(Vector3 start, Vector3 dir, float length, float headSize, Color color)
    {
        var end = start + dir.normalized * length;
        Debug.DrawLine(start, end, color);
        var orthogonal = Vector3.Cross(end - start, Vector3.forward).normalized * headSize / 2;
        var back = -(end - start).normalized * headSize / 2;
        Debug.DrawLine(end, end - orthogonal + back, color);
        Debug.DrawLine(end, end + orthogonal + back, color);
    }
    public static void DrawArrow(Vector3 start, Vector3 dir, float length, float headSize)
    {
        var end = start + dir.normalized * length;
        Debug.DrawLine(start, end);
        var orthogonal = Vector3.Cross(end - start, Vector3.forward).normalized * headSize / 2;
        var back = -(end - start).normalized * headSize / 2;
        Debug.DrawLine(end, end - orthogonal + back);
        Debug.DrawLine(end, end + orthogonal + back);
    }
}
