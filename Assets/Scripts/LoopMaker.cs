﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopMaker : MonoBehaviour
{
    public float LoopRegionWidth;
    public GameObject CameraObj;
    public GameObject Character;

    private GameObject _subCharacterLeft;
    private GameObject _subCharacterRight;

    // Start is called before the first frame update
    void Start()
    {
        if (!LoopRegionWidth.Equals(0f))
        {
            Vector2 pos = gameObject.transform.position;
            var leftObj = Instantiate(gameObject, pos + LoopRegionWidth * Vector2.right, Quaternion.identity);
            var rightObj = Instantiate(gameObject, pos - LoopRegionWidth * Vector2.right, Quaternion.identity);
            Destroy(leftObj.GetComponent<LoopMaker>());
            Destroy(rightObj.GetComponent<LoopMaker>());
            _subCharacterRight = Instantiate(Character, pos + LoopRegionWidth * Vector2.right, Quaternion.identity);
            _subCharacterLeft = Instantiate(Character, pos - LoopRegionWidth * Vector2.right, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 pos = gameObject.transform.position;
        var camPos = CameraObj.transform.position;
        if (camPos.x > pos.x + LoopRegionWidth / 2)
        {
            CameraObj.transform.position -= new Vector3(LoopRegionWidth, 0);
            Character.transform.position -= new Vector3(LoopRegionWidth, 0);
        }
        if (camPos.x < pos.x - LoopRegionWidth / 2)
        {
            CameraObj.transform.position += new Vector3(LoopRegionWidth, 0);
            Character.transform.position += new Vector3(LoopRegionWidth, 0);
        }

        var charPos = Character.transform.position;
        _subCharacterLeft.transform.position = charPos - LoopRegionWidth * Vector3.right;
        _subCharacterRight.transform.position = charPos + LoopRegionWidth * Vector3.right;
    }
}
