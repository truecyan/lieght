﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;

public class LightMap : MonoBehaviour
{
    [Range(-180f,0)]
    public float StartAngle = -45;
    [Range(0f, 360f)]
    public float AngleRange = 90;
    public float LightDistance = 5;
    public Vector2 Offset = Vector2.zero;
    public int Precision = 60;
    [Range(0f, 1f)]
    public float Alpha = 1f;
    public bool DarkLight;


    private int _lightInfoLayer;
    private int _checkLayer;

    private Vector2 _lightPos = Vector2.zero;
    private const float auxOffset = 0.001f;

    [SerializeField]private List<ActivationCtrl> _prevHitList = new List<ActivationCtrl>();

    private void Start()
    {
        _lightInfoLayer = LayerMask.GetMask("LightInfo");
        _checkLayer = LayerMask.GetMask("Check");
    }

    private float GetAngle(Vector2 point)
    {
        Vector2 down = -transform.up;
        Vector2 dir = point - _lightPos;

        return Vector2.SignedAngle(down, dir);
    }

    private static Vector2 Rotate(Vector2 v, float deg)
    {
        return Quaternion.Euler(0, 0, deg) * v;
    }

    private void OnValidate()
    {
        if (AngleRange < -StartAngle) AngleRange = -StartAngle;
        if (AngleRange + StartAngle > 180f) AngleRange = 180 - StartAngle;
    }

    //Restraint vertex position placed inside region of light
    //Delete point outside range
    //Add point which is placed in edge and meets with border of light.
    private Vector2[] TrimVertices(Vector2[] vertices)
    {
        float Sqr(float a)
        {
            return a * a;
        }
        float GetLength(Vector2 vertex)
        {
            return (vertex - _lightPos).magnitude;
        }
        List<Vector2> newVertices = new List<Vector2>();
        for (int i = 0; i < vertices.Length; i++)
        {
            if (GetLength(vertices[i]) > LightDistance)
            {
                int prev = (i + vertices.Length - 1) % vertices.Length;
                //if (GetLength(vertices[prev]) < LightDistance)
                //{
                var edge1 = vertices[i] - vertices[prev];
                var near1 = vertices[prev] - _lightPos;
                Vector2 proj1 = Vector3.Project(near1, edge1.normalized);
                var normal1 = near1 - proj1;
                if (Sqr(LightDistance) - normal1.sqrMagnitude > 0)
                {
                    var newPoint1 = Mathf.Sqrt(Sqr(LightDistance) - normal1.sqrMagnitude) * edge1.normalized + normal1;
                    newVertices.Add(newPoint1 + _lightPos);
                }
                
                //}
                int next = (i + 1) % vertices.Length;
                //if (GetLength(vertices[next]) < LightDistance)
                //{
                var edge2 = vertices[i] - vertices[next];
                var near2 = vertices[next] - _lightPos;
                Vector2 proj2 = Vector3.Project(near2, edge2.normalized);
                var normal2 = near2 - proj2;
                if (Sqr(LightDistance) - normal2.sqrMagnitude > 0)
                {
                    var newPoint2 = Mathf.Sqrt(Sqr(LightDistance) - normal2.sqrMagnitude) * edge2.normalized + normal2;
                    newVertices.Add(newPoint2 + _lightPos);
                }
                //}
            }
            else
            {
                if (i == 0)
                {
                    newVertices.Add(vertices[i]);
                }
                else
                {
                    var prevAngle = GetAngle(vertices[i - 1]);
                    var currAngle = GetAngle(vertices[i]);
                    var initAngle = GetAngle(vertices[0]);
                    if (prevAngle != currAngle && initAngle != currAngle)
                    {
                        newVertices.Add(vertices[i]);
                    }
                    else
                    {
                        if (prevAngle == currAngle)
                        {
                            if (GetLength(vertices[i - 1]) > GetLength(vertices[i]))
                                newVertices[newVertices.Count - 1] = vertices[i];
                        }

                        if (initAngle == currAngle)
                        {
                            if (GetLength(vertices[0]) > GetLength(vertices[i]))
                                newVertices[0] = vertices[i];
                        }
                    }
                }
            }
        }

        return newVertices.ToArray();
    }
    private void SortVertices(ref Vector2[] vertices)
    {
        Array.Sort(vertices, delegate (Vector2 x, Vector2 y)
        {
            var xAngle = GetAngle(x);
            var yAngle = GetAngle(y);
            if (xAngle < yAngle) return -1;
            if (xAngle > yAngle) return 1;
            var xDistSq = (x - _lightPos).sqrMagnitude;
            var yDistSq = (y - _lightPos).sqrMagnitude;
            if (xDistSq > yDistSq) return -1;
            if (xDistSq < yDistSq) return 1;
            return 0;
        });
    }
    private float FindEarliestAngle(Vector2[] vertices)
    {
        float earliestAngle = GetAngle(vertices[0]);
        for (int i = 1; i < vertices.Length; i++)
        {
            var angle = GetAngle(vertices[i]);
            if (angle < earliestAngle)
            {
                earliestAngle = angle;
            }
        }
        return earliestAngle;
    }
    private float FindLatestAngle(Vector2[] vertices)
    {
        float latestAngle = GetAngle(vertices[0]);
        for (int i = 1; i < vertices.Length; i++)
        {
            var angle = GetAngle(vertices[i]);
            if (angle > latestAngle)
            {
                latestAngle = angle;
            }
        }
        return latestAngle;
    }

    void FixedUpdate()
    {
        _lightPos = (Vector2)transform.position + Offset;
    }
    public Vector2 GetOrigin()
    {
        return _lightPos;
    }

    //counter-clockwise search using raycast
    //1. search counter-clockwise with raycast
    //  case 1: if raycast hit object
    //      1. found vertices of the object
    //      2. make vertices sorted (counter-clockwise from origin of light)
    //      3. raycast to all vertices until ray hit vertex.
    //      4. if raycast hit another object so didn't hit that vertex
    //        - return to case 1 and do this sequence with new object.
    //      5. else return to initial and start search with last vertex.
    //  case 2: if raycast didn't hit any object
    //      1. return to initial and continue search.
    public List<Vector2> GetLightData(ref List<ActivationCtrl> inLightListRes)
    {
        Physics2D.queriesStartInColliders = true;
        //LightPos = (Vector2)transform.position + Offset;
        float incr = 360f / Precision;
        float curr = StartAngle;
        Vector2 down = -transform.up;
        
        var lightData = new List<Vector2>();

        var hitList = new List<GameObject>();
        var inLightList = new List<ActivationCtrl>();
        var passList = new List<GameObject>();

        int prev = 0;

        void AddPoint(Vector2 point)
        {
            StylishDebug.DrawCross(point, 0.1f, Color.cyan);
            var idx = lightData.Count;
            if (idx > 0)
            {
                var prevAngle = GetAngle(lightData[prev]);
                var currAngle = GetAngle(point);
                if (prevAngle > currAngle + 0.05f && prev != 0 && currAngle > -180f)
                {
                    var prevVec = lightData[prev];
                    Debug.LogWarning("Invalid Point / Prev: " + prevAngle + " Curr: " + GetAngle(point) + " (Point: " + prevVec + "/" + point + ")");
                    //return;
                }
            }

            Vector2 delta = point - _lightPos;
            CheckObjects(delta.normalized, delta.magnitude, ref inLightList);
            lightData.Add(point);

            prev = idx;
        }

        //빛 경로에 있는 오브젝트를 리스트에 추가
        //모든 오브젝트의 Check 레이어에 있는 오브젝트는 일반 콜라이더.
        void CheckObjects(Vector2 dir, float distance, ref List<ActivationCtrl> list)
        {
            /*
            Physics2D.queriesHitTriggers = true;
            var triggerRays = Physics2D.RaycastAll(_lightPos, dir, distance, EnabledLayer | DisabledLayer);
            Physics2D.queriesHitTriggers = false;
            foreach (var ray in triggerRays)
            {
                if (ray.collider.isTrigger)
                {
                    var act = ray.transform.GetComponent<ActivationCtrl>();
                    if (act != null && !list.Contains(act))
                    {
                        list.Add(act);
                    }
                }
            }
            */
            var offset = 0.01f;
            var checkRays = Physics2D.RaycastAll(_lightPos, dir, distance + offset, _checkLayer);
            foreach (var ray in checkRays)
            {
                var act = ray.transform.GetComponent<ActivationCtrl>();
                if (act != null && !list.Contains(act))
                {
                    list.Add(act);
                }
            }
        }

        var infLoopCount = 0;
        void FindPointForObj(GameObject obj) //목표 오브젝트 방향으로 뻗어나간 빛이 진행할 한계점 모두 찾기
        {
            //아무리 그래도 10번 걸릴 일은 없겠지
            if (hitList.Contains(obj))
            {
                infLoopCount++;
                if (infLoopCount > 10)
                {
                    Debug.LogWarning("Infinite Loop Warning");
                    return;
                }
            }
            else
            {
                infLoopCount = 0;
            }

            hitList.Add(obj);

            var rawVertices = obj.GetComponent<ColVert>().GetVertices(); //해당 오브젝트의 콜라이더의 꼭지점 받아오기
            var vertices = TrimVertices(rawVertices); //빛 영역 안에 있는 점들만 고려하기
            SortVertices(ref vertices); //반시계방향으로 진행하기 위해 정렬

            var initialPoint = true;
            var continueCnt = 0;
            for (int i = 0; i < vertices.Length; i++)
            {
                var vertex = vertices[i];
                StylishDebug.DrawSquare(vertex, 0.1f);
                var dest = GetAngle(vertex); //빛의 진행 거리를 조사할 목표 꼭지점의 각도
                //Debug.Log(dest);
                if (dest > StartAngle + AngleRange)
                {
                    curr = dest;
                    return;
                }//목표 꼭지점이 범위를 벗어났을 경우 종료

                //이미 점을 구한 영역을 또 조사하면 안되므로
                //마지막으로 추가한 점의 각도를 구한 뒤
                //목표 꼭지점의 각도가 이보다 작을 경우 스킵한다.
                //맨 처음에는 이 과정을 스킵한다.
                var prevAngle = GetAngle(lightData[prev]);
                if (prevAngle >= 180) prevAngle -= 360;

                if ((prevAngle + 0.01f >= dest || prevAngle + 180 < dest) && lightData.Count > 0 && prev != 0)
                {
                    //Debug.Log("Pass: " +prevAngle);
                    //Debug.Log("done: " + lightData[prev] + "/angle:" + prevAngle);
                    //Debug.Log("try: " + vertex + "/angle:" + dest);
                    continueCnt++;
                    continue;
                }
                //ActivateSimulation - vertex
                var vertDir = (vertex - _lightPos).normalized;
                var vertexRay = Physics2D.Raycast(_lightPos, vertDir, LightDistance, _lightInfoLayer);
                
                if (!initialPoint)
                {
                    for (var searchAngle = prevAngle + incr; searchAngle < dest; searchAngle += incr)
                    {
                        var searchDir = Rotate(down, searchAngle);
                        var searchRay = Physics2D.Raycast(_lightPos, searchDir, LightDistance, _lightInfoLayer);
                        CheckObjects(searchDir, searchRay.distance, ref inLightList);

                        if (searchRay)
                        {
                            StylishDebug.DrawCross(searchRay.point, 0.1f, Color.red);
                            var searchObj = searchRay.transform.gameObject;
                            if (searchObj != obj)
                            {
                                FindPointForObj(searchObj);
                                return;
                            }
                        }
                    }
                }
                
                initialPoint = false;
                
                curr = dest; //탐색하고 있는 각도를 표시하는 변수를 목표 꼭지점의 각도로 업데이트한다.

                if (vertexRay)
                {
                    var vertexRayPoint = vertexRay.point;
                    var auxPlusDir = Rotate(vertDir, auxOffset);
                    var auxMinusDir = Rotate(vertDir, -auxOffset);
                    var auxPlusRay = Physics2D.Raycast(_lightPos, auxPlusDir, LightDistance, _lightInfoLayer);
                    var auxMinusRay = Physics2D.Raycast(_lightPos, auxMinusDir, LightDistance, _lightInfoLayer);
                    //꼭지점 방향으로 쏜 레이가 목표 오브젝트에 닿았을 경우
                    if (vertexRay.transform.gameObject == obj)
                    {
                        //살짝 각도를 줄여 쏜 레이가 아무곳에도 닿지 않았을 경우
                        if (!auxMinusRay)
                        {
                            //이 꼭지점은 빛이 진행할 수 있는 거리가 바뀌는 경계점이며, 다른 오브젝트에 막혀있지는 않아 끝까지 진행한다.
                            //해당 각도로 진행했을 때 빛이 끝나는 지점에 점을 추가해준다.
                            var endPoint = _lightPos + vertDir * LightDistance;
                            if ((vertexRayPoint - endPoint).magnitude > auxOffset * vertexRay.distance)
                            {
                                //영역의 경계에 있는 점이 아닐 때만 점을 추가한다.
                                AddPoint(endPoint);
                            }
                        }
                        //혹은 목표로 하는 꼭지점이 아닌 곳에 닿았을 경우
                        else
                        {
                            var auxMinusPoint = auxMinusRay.point;
                            //오차 거리를 l = r * theta의 180/pi배로 근사.(대략 89도까지의 기울어짐 고려)
                            if ((vertexRayPoint - auxMinusPoint).magnitude > auxOffset * vertexRay.distance)
                            {
                                //이 꼭지점은 빛이 진행할 수 있는 거리가 바뀌는 경계점이며, 다른 오브젝트에 막혀있다.
                                //레이가 닿은 지점에 점을 추가해준다.
                                var auxMinusObj = auxMinusRay.transform.gameObject;
                                if (hitList.Contains(auxMinusObj)) AddPoint(auxMinusPoint);
                                else
                                {
                                    FindPointForObj(auxMinusObj);
                                    return;
                                }
                            }
                        }
                        StylishDebug.DrawDiamond(vertexRayPoint, 0.2f, Color.white);
                        //목표 꼭지점을 향해 빛을 쏜 것이므로 항상 이 방향으로 쏜 레이가 닿은 점은 추가되어야 한다.
                        //레이가 닿은 지점에 점 추가
                        AddPoint(vertexRayPoint);

                        //살짝 각도를 늘려 쏜 레이가 아무곳에도 닿지 않았을 경우
                        if (!auxPlusRay)
                        {
                            //해당 각도로 진행했을 때 빛이 끝나는 지점에 점 추가
                            var endPoint = _lightPos + vertDir * LightDistance;
                            if ((vertexRayPoint - endPoint).magnitude > auxOffset * vertexRay.distance)
                            {
                                //영역의 경계에 있는 점이 아닐 때만 점을 추가한다.
                                AddPoint(endPoint);
                            }
                            return;//이 오브젝트가 존재하는 영역에 대한 점 조사가 끝남.
                        }
                        //혹은 목표로 하는 꼭지점이 아닌 곳에 닿았을 경우
                        var auxPlusPoint = auxPlusRay.point;
                        //오차 거리를 l = r * theta의 180/pi배로 근사.(대략 89도까지의 기울어짐 고려)
                        if ((vertexRayPoint - auxPlusPoint).magnitude > auxOffset * vertexRay.distance)
                        {
                            //이 꼭지점은 빛이 진행할 수 있는 거리가 바뀌는 경계점이며, 오브젝트에 막혀있다.
                            //레이가 닿은 지점에 점 추가
                            AddPoint(auxPlusPoint);
                        }
                        //레이가 다른 오브젝트에 닿았을 경우
                        if (auxPlusRay.transform.gameObject != obj)
                        {
                            //목표 오브젝트에 대한 탐색은 끝난 것이므로
                            //닿은 오브젝트에 대해 탐색 시작
                            FindPointForObj(auxPlusRay.transform.gameObject);
                            return;
                        }
                    }
                    //꼭지점 방향으로 쏜 레이가 다른 오브젝트에 닿았을 경우
                    else
                    {
                        //각도를 살짝 줄여 쏜 레이가 목표로 한 오브젝트에 닿았을 경우
                        if (auxMinusRay && auxMinusRay.transform.gameObject == obj)
                        {
                            //부동소수점 오차로 인해 해당 오브젝트의 꼭지점에 닿지 못한 것이므로
                            //먼저 목표로 한 꼭지점을 추가해준다. 
                            if ((auxMinusRay.point - vertex).magnitude < auxOffset * auxMinusRay.distance)
                            {
                                var point = vertex;
                                AddPoint(point);
                            }

                            //레이가 닿은 오브젝트는 다음으로 탐색해야 할 오브젝트이다.
                            //레이가 닿은 지점에 점을 추가하고 다음 오브젝트를 탐색한다.
                            var otherPoint = vertexRay.point;
                            AddPoint(otherPoint);
                            StylishDebug.DrawDiamond(otherPoint, 0.225f, Color.green);
                            FindPointForObj(vertexRay.transform.gameObject);
                            return;
                        }
                        //각도를 살짝 늘려 쏜 레이가 목표로 한 오브젝트에 닿았을 경우
                        if (auxPlusRay && auxPlusRay.transform.gameObject == obj)
                        {
                            //부동소수점 오차로 인해 해당 오브젝트의 꼭지점에 닿지 못한 것이므로
                            //레이가 닿은 지점에 점을 추가한 다음 목표로 한 꼭지점을 추가해준다. 
                            var otherPoint = vertexRay.point;
                            AddPoint(otherPoint);
                            if ((auxPlusRay.point - vertex).magnitude < auxOffset * auxPlusRay.distance)
                            {
                                var point = vertex;
                                AddPoint(point);
                                StylishDebug.DrawDiamond(point, 0.25f, Color.green);
                            }
                        }
                        else
                        {
                            var initialTrimmed = TrimVertices(rawVertices);
                            int len = initialTrimmed.Length;
                            //나머지 모든 점에 대해서 겹침 여부 체크하기
                            for (var j = i; j < vertices.Length; j++)
                            {
                                var currVertex = vertices[j];
                                int idx;

                                for (idx = 0; idx < len; idx++)
                                {
                                    if ((currVertex - initialTrimmed[idx]).magnitude < 0.01f) break;
                                }

                                var prevVertex = initialTrimmed[(idx + len - 1) % len];
                                //이 레이가 닿은 점이 오브젝트가 겹친 지점.
                                var edgeCastAll = Physics2D.LinecastAll(prevVertex, currVertex);

                                foreach (var col in edgeCastAll)
                                {
                                    if (col.collider.Equals(vertexRay.collider))
                                    {
                                        var checkerRay = Physics2D.Linecast(_lightPos, col.point);
                                        if (checkerRay && (checkerRay.point - col.point).magnitude < 0.01f)
                                        {
                                            //겹친 지점에 점을 추가하고 다음 오브젝트 탐색 시작.
                                            Debug.DrawLine(prevVertex, currVertex, Color.red);
                                            AddPoint(col.point);
                                            FindPointForObj(vertexRay.transform.gameObject);
                                            return;
                                        }
                                        break;
                                    }
                                }
                            }

                            //목표로 한 꼭지점을 다른 오브젝트가 가리고 있는 것이므로 목표 오브젝트에 대한 탐색은 끝난것이다.
                            //다음 오브젝트 탐색 시작.
                            FindPointForObj(vertexRay.transform.gameObject);
                            return;
                        }
                    }
                }
                //꼭지점 방향으로 쏜 레이가 어디에도 닿지 않았을 경우
                else
                {
                    //해당 꼭지점이 빛 영역 안에 있을 경우 (TrimVertices 함수가 제곱근 연산을 하므로 오차가 있을 수 있어 0.001을 더함.)
                    if ((vertex - _lightPos).magnitude <= LightDistance + 0.001f)
                    {
                        var endPoint = _lightPos + Rotate(down, GetAngle(vertex)) * LightDistance;
                        var auxPlusDir = Rotate(vertDir, auxOffset);
                        var auxMinusDir = Rotate(vertDir, -auxOffset);
                        var auxPlusRay = Physics2D.Raycast(_lightPos, auxPlusDir, LightDistance, _lightInfoLayer);
                        var auxMinusRay = Physics2D.Raycast(_lightPos, auxMinusDir, LightDistance, _lightInfoLayer);

                        //살짝 각도를 줄여 쏜 레이가 목표로 한 오브젝트에 닿았을 경우
                        if (auxMinusRay && auxMinusRay.transform.gameObject == obj)
                        {
                            //부동소수점 오차로 인해 해당 오브젝트의 꼭지점에 닿지 못한 것이므로
                            //먼저 목표로 한 꼭지점을 추가해준다. 
                            var point = vertex;
                            if ((point - endPoint).magnitude > auxOffset * LightDistance)
                            {
                                //영역의 경계에 있는 점이 아닐 때만 점을 추가한다.
                                AddPoint(point);
                            }
                        }

                        //이 꼭지점은 빛이 진행할 수 있는 거리가 바뀌는 경계점이며, 다른 오브젝트에 막혀있지는 않아 끝까지 진행한다.
                        //해당 각도로 진행했을 때 빛이 끝나는 지점에 점을 추가해준다. 
                        AddPoint(endPoint);

                        //각도를 살짝 늘려 쏜 레이가 목표로 한 오브젝트에 닿았을 경우
                        if (auxPlusRay && auxPlusRay.transform.gameObject == obj)
                        {
                            //부동소수점 오차로 인해 해당 오브젝트의 꼭지점을 넘어간 것이므로
                            //목표로 한 꼭지점을 추가해준다.
                            var point = vertex;
                            if ((point - endPoint).magnitude > auxOffset * LightDistance)
                            {
                                //영역의 경계에 있는 점이 아닐 때만 점을 추가한다.
                                AddPoint(point);
                            }
                        }
                    }
                    //영역 바깥일 경우
                    else
                    {
                        //영역 바깥의 꼭지점을 제거하는 작업을 해서 이 분기로 들어올 수 있는지는 모르겠지만
                        //암튼 걸린다면 어차피 안닿아야 하는 꼭지점이니까
                        //해당 각도로 진행했을 때 빛이 끝나는 지점에 점을 추가하고 목표 오브젝트에 대한 탐색을 끝낸다.
                        var endPoint = _lightPos + Rotate(down, GetAngle(vertex)) * LightDistance;
                        AddPoint(endPoint);
                        Debug.LogWarning("[WARN] LightMesh: Trimming may not valid.");
                        return;
                    }
                }
            }
            if (continueCnt == vertices.Length)
            {
                passList.Add(obj);
            }
            //성공적으로 반복문이 끝났다면 오브젝트의 콜라이더의 꼭지점 중 가장 각도가 큰 꼭지점까지 조사가 완료되어 있을 것이다. 
        }
        while (true)
        {
            if (curr > StartAngle + AngleRange) curr = StartAngle + AngleRange;
            var dir = Rotate(down, curr);
            //ActivateSimulation - main loop
            //맨 처음 닿은 1개만 활성화 할 경우 겹쳐있는 오브젝트를 못찾을 수 있음.
            //전부 활성화 할 경우 닿을 수 없는 오브젝트를 활성화 할 수 있음
            //가장 빠른 각도를 포함한 오브젝트가 탐색 범위 내일 경우 해당 오브젝트를 활성화.
            //범위 바깥일 경우 가장 먼저 닿은 걸 활성화.
            var rays = Physics2D.RaycastAll(_lightPos, dir, LightDistance, _lightInfoLayer);
            if (rays.Length > 0)
            {
                GameObject earliestObj = rays[0].transform.gameObject;
                var earliestVertices = earliestObj.GetComponent<ColVert>().GetVertices();
                var initialTrimmed = TrimVertices(earliestVertices);

                if (initialTrimmed.Length > 0)
                {
                    var earliestAngle = FindEarliestAngle(initialTrimmed);
                    var latestAngle = FindLatestAngle(initialTrimmed);
                    for (int i = 1; i < rays.Length; i++)
                    {
                        var obj = rays[i].transform.gameObject;

                        var vertices = obj.GetComponent<ColVert>().GetVertices(); //TODO: Optimize with caching
                        var trimmed = TrimVertices(vertices);
                        if (trimmed.Length == 0) continue;
                        var earlyAngle = FindEarliestAngle(trimmed);
                        if (earlyAngle < earliestAngle && earlyAngle > StartAngle)
                        {
                            earliestAngle = earlyAngle;
                            latestAngle = FindLatestAngle(trimmed);
                            earliestObj = obj;
                        }
                    }

                    if (passList.Contains(earliestObj))
                    {
                        AddPoint(rays[0].point);
                    }
                    else
                    {
                        if (prev == 0 && (earliestAngle < StartAngle || latestAngle > StartAngle + 180))
                        {
                            AddPoint(rays[0].point);
                            FindPointForObj(rays[0].transform.gameObject);
                        }
                        else
                        {
                            FindPointForObj(earliestObj);
                        }
                    }
                }
                else
                {
                    var point = _lightPos + dir * LightDistance;
                    StylishDebug.DrawSquare(point, 0.1f,Color.black);
                    AddPoint(point);
                }
            }
            else
            {
                var point = _lightPos + dir * LightDistance;

                AddPoint(point);
            }

            if (curr < StartAngle + AngleRange) curr += incr;
            else
            {
                curr = StartAngle + AngleRange;
                var lastDir = Rotate(down, curr);
                var lastRay = Physics2D.Raycast(_lightPos, lastDir, LightDistance, _lightInfoLayer);
                if (lastRay)
                {
                    AddPoint(lastRay.point);
                }
                break;
            }
        }
        Physics2D.queriesStartInColliders = false;
        inLightListRes = inLightList;
        return lightData;
    }

    public List<Vector2> GetLight()
    {
        var inLightList = new List<ActivationCtrl>();
        var res = GetLightData(ref inLightList);

        var addedList = inLightList.Except(_prevHitList);
        var deletedList = _prevHitList.Except(inLightList);

        foreach (var elem in addedList)
        {
            if(DarkLight) elem.AddDarkLight(this);
            else elem.AddLight(this);
        }

        foreach (var elem in deletedList)
        {
            if(DarkLight) elem.DeleteDarkLight(this);
            else elem.DeleteLight(this);
        }
        _prevHitList = inLightList;
        if (LightManager.Instance.CheckUpdate())
        {
            res = GetLightData(ref inLightList);
        }
        return res;
    }


    public void LightOn()
    {
        LightManager.Instance.Lights.Add(this);
    }

    public void LightOff()
    {
        LightManager.Instance.Lights.Remove(this);
        foreach (var elem in _prevHitList)
        {
            if (elem == null) continue;
            if(DarkLight) elem.DeleteDarkLight(this);
            else elem.DeleteLight(this);
        }
        _prevHitList.Clear();
    }

    private void OnDestroy()
    {
        if (LightManager.Instance != null)
            LightOff();
    }
}
