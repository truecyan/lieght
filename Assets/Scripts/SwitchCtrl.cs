﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCtrl : MonoBehaviour
{
    public Sprite LightOn;
    public Sprite LightOff;

    public SpriteRenderer LightSprite;

    public string SwitchSymbol;

    public bool State;
    
    // Start is called before the first frame update
    void Start()
    {
        SetState(false);
    }

    public void InvertState()
    {
        SetState(!State);
    }

    public void SetState(bool state)
    {

        State = state;

        if (state)
        {
            LightSprite.sprite = LightOn;
        }
        else
        {
            LightSprite.sprite = LightOff;
        }

        //이벤트 발생 후 맵에 존재하는 모든 전등 영향 --> 지정된 문양을 가진 전등만 반응
        LightManager.Instance.SwitchUpdate(SwitchSymbol, state);
    }
}
