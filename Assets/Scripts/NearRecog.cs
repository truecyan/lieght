﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NearRecog : MonoBehaviour
{
    public float RecogRadius = 1.25f;
    public float RecogSmallRadius = 0.75f;
    public Vector2 RecogOffset = new Vector2(0,0.25f);
    public Vector2 RecogSmallOffset = new Vector2(0, -0.125f);
    public float RecogAlpha = 0.1f;
    public int RecogPrecision = 60;

    private LightMap _lm;

    // Start is called before the first frame update
    void Start()
    {
        _lm = GetComponent<LightMap>();
        _lm.LightOn();
        _lm.LightDistance = RecogRadius;
        _lm.Precision = RecogPrecision;
        _lm.Alpha = RecogAlpha;
    }
    public void SmallRecog(bool state)
    {
        if (state)
        {
            _lm.LightDistance = RecogSmallRadius;
            _lm.Offset = RecogSmallOffset;
        }
        else
        {
            _lm.LightDistance = RecogRadius;
            _lm.Offset = RecogOffset;
        }
    }
}
