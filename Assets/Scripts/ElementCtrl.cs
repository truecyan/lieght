﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementCtrl : MonoBehaviour
{
    public GameObject PhysicsObj;
    public GameObject LightInfoObj;

    public void SetActive(bool state)
    {
        LightInfoObj.SetActive(state);
        PhysicsObj.SetActive(state);
    }
}
